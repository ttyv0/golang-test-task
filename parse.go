package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"github.com/gorilla/context"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
)

type (
	requestType struct {
		Site   []string
		Search string
	}

	responseType struct {
		FoundAt []string
	}

	channelParam struct {
		site   string
		search string
	}
)

func parseHandler(w http.ResponseWriter, r *http.Request) {
	var val1, val2 interface{}
	var ok bool
	if val1, ok = context.GetOk(r, keyDB); !ok {
		log.Fatal("Bad context")
	}
	if val2, ok = context.GetOk(r, tableNameKey); !ok {
		log.Fatal("Very bad context")
	}

	db := val1.(*sql.DB)
	tableName := val2.(string)

	decoder := json.NewDecoder(r.Body)

	defer func() {
		err := r.Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	request := new(requestType)
	err := decoder.Decode(request)
	if err != nil {
		log.Fatal(err)
	}

	wg := new(sync.WaitGroup)
	ch := make(chan *channelParam)
	done := make(chan struct{})

	go dbCommitter(db, tableName, ch, done)
	for _, site := range request.Site {
		wg.Add(1)
		go siteScanner(ch, site, request.Search, wg)
	}

	wg.Wait()
	close(ch)
	<-done
	close(done)

	rows, err := db.Query("SELECT url FROM "+tableName+" WHERE search = $1", request.Search)
	if err != nil {
		log.Fatal(err)
	}
	output := make([]string, 0)
	for rows.Next() {
		var siteURL string
		if err = rows.Scan(&siteURL); err != nil {
			log.Fatal(err)
		}
		output = append(output, siteURL)
	}

	err = json.NewEncoder(w).Encode(&responseType{FoundAt: output})
	if err != nil {
		log.Fatal(err)
	}
}

func dbCommitter(db *sql.DB, tableName string, ch <-chan *channelParam, done chan<- struct{}) {
	defer func() { done <- struct{}{} }()
	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	for param := range ch {
		_, err = tx.Exec("INSERT INTO "+tableName+" VALUES($1, $2)", param.site, param.search)
		if err != nil {
			log.Fatal(err)
		}
	}
	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
	}
}

func siteScanner(ch chan<- *channelParam, site string, search string, wg *sync.WaitGroup) {
	defer wg.Done()
	page, err := http.Get(site)
	if err != nil {
		log.Println(err)
		return
	}

	defer func() {
		err = page.Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	bs, err := ioutil.ReadAll(page.Body)
	if err != nil {
		log.Println(err)
		return
	}

	if bytes.Contains(bs, []byte(search)) {
		ch <- &channelParam{site, search}
	}
}
