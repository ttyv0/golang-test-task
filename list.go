package main

import (
	"database/sql"
	"github.com/gorilla/context"
	"html/template"
	"log"
	"net/http"
)

type (
	item struct {
		URL    string
		Search string
	}
)

const listTemplate = `<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
	</head>
	<body>
		{{range .}}<div><a href="{{ .URL }}">{{ .URL  }}</a> {{ .Search }}</div><br>{{"\n		"}}
		{{else}}<div><strong>This place does not contain anything good. =(</strong></div>{{end}}
	</body>
</html>
`

var tmpl *template.Template

func listHandler(w http.ResponseWriter, r *http.Request) {
	var val1, val2 interface{}
	var ok bool
	if val1, ok = context.GetOk(r, keyDB); !ok {
		log.Fatal("Bad context")
	}
	if val2, ok = context.GetOk(r, tableNameKey); !ok {
		log.Fatal("Very bad context")
	}

	db := val1.(*sql.DB)
	tableName := val2.(string)

	var err error
	if tmpl == nil {
		tmpl, err = template.New("List").Parse(listTemplate)
		if err != nil {
			log.Fatal(err)
		}
	}

	rows, err := db.Query("SELECT url, search FROM " + tableName + ";")
	if err != nil {
		log.Fatal(err)
	}
	items := make([]item, 0)
	for rows.Next() {
		var siteURL, search string
		if err = rows.Scan(&siteURL, &search); err != nil {
			log.Fatal(err)
		}
		items = append(items, item{siteURL, search})
	}
	err = tmpl.Execute(w, items)
	if err != nil {
		log.Fatal(err)
	}
}
