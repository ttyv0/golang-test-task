package main

import (
	"database/sql"
	"errors"
	"github.com/gorilla/context"
	_ "github.com/lib/pq"
	"net/http"
	"net/url"
	"os"
)

type key int

const keyDB key = 0
const tableNameKey = 1

const (
	defaultUser     = "postgres"
	defaultPassword = defaultUser
	defaultHost     = "localhost"
	defaultScheme   = defaultUser
	defaultPath     = defaultUser
)

func initDB(driverDB string, source string, tableName string) (*sql.DB, error) {
	if driverDB == "" || source == "" || tableName == "" {
		return nil, errors.New("driverDB, source and tableName can't be an empty string")
	}
	db, err := sql.Open(driverDB, source)
	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	//TODO SELECT to_regclass('search_result');
	_, err = db.Exec("CREATE TABLE IF NOT EXISTS " + tableName + " ( url text NOT NULL, search text NOT NULL );")
	if err != nil {
		return nil, err
	}
	return db, nil
}

func middlewareDB(next http.HandlerFunc, db *sql.DB, tableName string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		context.Set(r, keyDB, db)
		context.Set(r, tableNameKey, tableName)
		next.ServeHTTP(w, r)
	})
}

func getDBSource() string {
	var password, host string
	if password = os.Getenv("TEST_TASK_DB_ENV_POSTGRES_PASSWORD"); password == "" {
		password = defaultPassword
	}
	if host = os.Getenv("TEST_TASK_DB_PORT_5432_TCP_ADDR"); host == "" {
		host = defaultHost
	}
	u := new(url.URL)
	u.Scheme = defaultScheme
	u.User = url.UserPassword(defaultUser, password)
	u.Host = host
	u.Path = defaultPath
	q := u.Query()
	q.Set("sslmode", "disable")
	u.RawQuery = q.Encode()
	return u.String()
}
