package main

import (
	"net/http"
	"os"
	"sync"
	"testing"
)

const (
	defaultSource  = "postgres://postgres:postgres@localhost/postgres?sslmode=disable"
	defaultSource2 = "postgres://postgres:test-password@test-host/postgres?sslmode=disable"
	testTableName  = "test_table"
	testString     = "testString"
	testSiteURL    = "http://0.0.0.0:8088/testsite"
)

var (
	testSiteNames = [3]string{"testSite1", "testSite2", "testSite3"}
)

func TestGetDBSource(t *testing.T) {
	source := getDBSource()
	if source != defaultSource {
		t.Error("Incorrect dbSource: ", source)
	}

	if err := os.Setenv("TEST_TASK_DB_ENV_POSTGRES_PASSWORD", "test-password"); err != nil {
		t.Error(err)
	}
	if err := os.Setenv("TEST_TASK_DB_PORT_5432_TCP_ADDR", "test-host"); err != nil {
		t.Error(err)
	}

	source = getDBSource()
	if source != defaultSource2 {
		t.Error("Incorrect dbSource: ", source)
	}
	os.Clearenv()
}

func TestInitDB(t *testing.T) {
	db, err := initDB("postgres", getDBSource(), testTableName)
	if err != nil {
		t.Fatal(err)
	}

	var tableName interface{}
	err = db.QueryRow("SELECT to_regclass('" + testTableName + "');").Scan(&tableName)
	if err != nil {
		t.Error(err)
	}

	if tableName == nil {
		t.Fatal("Table is't exist")
	}

	if string(tableName.([]uint8)) != testTableName {
		t.Errorf("Incorrect table name: %s", tableName)
	}
}

func TestDBCommitter(t *testing.T) {
	ch := make(chan *channelParam)
	done := make(chan struct{})
	db, err := initDB("postgres", getDBSource(), testTableName)
	if err != nil {
		t.Fatal(err)
	}

	_, err = db.Exec("DELETE FROM " + testTableName + ";")
	if err != nil {
		t.Fatal(err)
	}

	go dbCommitter(db, testTableName, ch, done)
	ch <- &channelParam{testSiteNames[0], testString}
	ch <- &channelParam{testSiteNames[1], testString}
	ch <- &channelParam{testSiteNames[2], testString}
	close(ch)
	<-done

	rows, err := db.Query("SELECT url FROM "+testTableName+" WHERE search = $1;", testString)
	if err != nil {
		t.Fatal(err)
	}

	testSites := make([]string, 0)
	for rows.Next() {
		var u string
		err = rows.Scan(&u)
		if err != nil {
			t.Error(err)
		}
		testSites = append(testSites, u)
	}

	if len(testSites) != 3 {
		t.Fatalf("Incorrect testSites size: %d", len(testSites))
	}

	if testSites[0] != testSiteNames[0] || testSites[1] != testSiteNames[1] || testSites[2] != testSiteNames[2] {
		t.Error("Incorrect data in db")
	}
}

func TestSiteScanner(t *testing.T) {
	var err error
	http.HandleFunc("/testsite", func(w http.ResponseWriter, req *http.Request) {
		_, err = w.Write([]byte("site1"))
		if err != nil {
			t.Fatal(err)
		}
	})

	go func() {
		err = http.ListenAndServe(":8088", nil)
	}()
	if err != nil {
		t.Fatal(err)
	}

	ch := make(chan *channelParam)
	done := make(chan struct{})
	testParam := make([]*channelParam, 0)
	go func() {
		for param := range ch {
			testParam = append(testParam, param)
		}
		done <- struct{}{}
	}()

	wg := new(sync.WaitGroup)
	wg.Add(2)

	go siteScanner(ch, testSiteURL, "site1", wg)
	go siteScanner(ch, testSiteURL, "test", wg)

	wg.Wait()
	close(ch)
	<-done
	close(done)

	if len(testParam) != 1 {
		t.Fatal("Incorrect testParam size: ", len(testParam))
	}

	if testParam[0].site != testSiteURL || testParam[0].search != "site1" {
		t.Error("incorrect testParam")
	}

}
