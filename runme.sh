#!/bin/sh
docker build -t test-task-golang-app-image .
docker run --name test-task-db -d postgres
docker run --name test-task-golang-app --link test-task-db:test-task-db -d -p 80:8080 test-task-golang-app-image
