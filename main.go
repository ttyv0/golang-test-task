package main

import (
	"github.com/gorilla/mux"

	"log"
	"net/http"
)

func main() {
	var tableName = "search_result"
	db, err := initDB("postgres", getDBSource(), tableName)
	if err != nil {
		log.Fatal(err)
	}

	r := mux.NewRouter()

	r.Handle("/list", middlewareDB(listHandler, db, tableName)).Methods("GET")

	r.Handle("/parse", middlewareDB(parseHandler, db, tableName)).Methods("POST")

	err = http.ListenAndServe(":8080", r)
	if err != nil {
		log.Fatalln(err)
	}
}
